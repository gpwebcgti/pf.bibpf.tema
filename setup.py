# -*- coding: utf-8 -*-

from setuptools import find_packages
from setuptools import setup

version = '1.1.3'
long_description = (
    open('README.rst').read() + '\n' +
    open('CONTRIBUTORS.rst').read() + '\n' +
    open('CHANGES.rst').read()
)

setup(
    name='pf.bibpf.tema',
    version=version,
    description="Tema baseado no IDG para a Biblioteca Digital da Polícia Federal ",
    long_description=long_description,
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Environment :: Web Environment",
        "Framework :: Plone",
        "Framework :: Plone :: 4.3",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Programming Language :: JavaScript",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2.7",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        "Topic :: Multimedia",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    keywords='plone .gov.br temas diazo dpf',
    author='Servidores da Polícia Federal',
    author_email='gpweb.cgti@dpf.gov.br',
    url='https://gitlab.dpf.gov.br/produtos-plone-pip/pf.bibpf.tema.git',
    license='General Public License - GPL (“Licença Pública Geral”), versão 3.0, em português',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    namespace_packages=['pf', 'pf.bibpf'],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'brasil.gov.portal',
        'Products.CMFPlone >=4.3',
        'setuptools',
        'plone.app.themingplugins',
        'plone.app.contenttypes<=1.1b5',
    ],
    extras_require={
        'test': [
            'plone.app.testing',
        ]
    },
    entry_points="""
    [z3c.autoinclude.plugin]
    target = plone
    """,
)
