Histórico de Alterações
------------------------

1.1.3 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Corrige Bug no link deixado pela Versão Anterior 
  [luiz.lgsb]

1.1.2 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Adiciona link no logo da barra de topo 
  [luiz.lgsb]

1.1.1b 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Corrige o arquivo de preview do tema
  [luiz.lgsb]

1.1.1 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Correção dos nomes de pacote no setup.py
  [luiz.lgsb]

1.1.0 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Primeira versão marcada (TAG) após o Fork. 
  [luiz.lgsb]

1.0.0 (unreleased)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Primeira versão (Fork de pf.internet.tema)
  [luiz.lgsb]
