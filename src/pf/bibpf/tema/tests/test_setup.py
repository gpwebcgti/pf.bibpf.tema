# -*- coding: utf-8 -*-

from brasil.gov.temas.testing import INTEGRATION_TESTING
from plone.app.theming.utils import getTheme

import unittest


class InstallTestCase(unittest.TestCase):

    layer = INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']

    def test_tema_preto_disponivel(self):
        theme = getTheme('preto')
        self.assertTrue(theme is not None)
        self.assertEqual(theme.__name__, 'preto')
        self.assertEqual(theme.title, 'Portal da Polícia Federal - Tema Preto com Dourado')
        self.assertEqual(theme.description,
                         'Tema para Portal Internet da Polícia Federal')
        self.assertEqual(theme.rules, '/++theme++preto/rules.xml')
        self.assertEqual(theme.absolutePrefix, '/++theme++preto')
        self.assertEqual(theme.doctype, "<!DOCTYPE html>")
