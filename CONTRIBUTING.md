Template para Issue

Description
===========
Descrição geral do problema.

Steps to reproduce the issue:
=============================
Como reproduzir o problema.

Results you received:
=====================
Resultados que você recebeu quando reproduziu os passos acima.

Results you expected:
=====================
Resultados que você esperava.

Versions:
========
Versões dos sistemas e pacotes usados no ambiente.
