**************************************************************************
pf.bibpf.tema: Pacote de tema para Biblioteca Digital da Polícia Federal
**************************************************************************

.. contents:: Conteúdo
   :depth: 2

Introdução
----------

Este pacote provê um tema preto com dourado com o Brasão da Polícia Federal, 
baseado na Identidade Digital do Governo Federal para uso na Biblioteca 
Digital da Polícia Federal.


Instalação
----------

Para habilitar a instalação deste produto em um ambiente que utilize o
buildout:

1. Editar o arquivo buildout.cfg (ou outro arquivo de configuração) e
   adicionar o pacote ``pf.bibpf.tema`` à lista de eggs da instalação:

.. code-block:: ini

    [buildout]
    ...
    eggs =
        pf.bibpf.tema

2. Após alterar o arquivo de configuração é necessário executar
   ''bin/buildout'', que atualizará sua instalação.

3. Reinicie o Plone

4. Acesse o painel de controle e na opção **Temas** você verá o tema
provido por este pacote listado.
